﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int speed;
    public Sprite idle;
    public Sprite go_up;
    public Sprite go_down;

    public GameObject Laser;
    public GameObject shot;
    public GameObject doubleShot;
    public GameObject[] shots;
    public GameObject[] lasers;
    public bool isLaser;
    public bool isDouble;
    
    private int bonus_rank;
    private bool bonus_up;
    public GameObject Speedup_active;
    public GameObject Missile_active;
    public GameObject Double_active;
    public GameObject Laser_active;
    public GameObject Option_active;
    public GameObject Shield_active;
    void Start()
    {
        bonus_up = false;
        bonus_rank = 0;
        isLaser = false;
        isDouble = false;
    }
    
    void Update()
    {
        shots = GameObject.FindGameObjectsWithTag("PlayerShot");
        lasers = GameObject.FindGameObjectsWithTag("PlayerLaser");
        Movement();
        Shoot();
        Bonus_bar();
        if (Input.GetKeyDown(KeyCode.D))
            Bonus_use();
    }

    //si le joueur appuie sur espace et qu'il y a moins de 2 tirs à l'écran un tir part
    void Shoot()
    {
        if (Input.GetKeyDown(KeyCode.F) && shots.Length < 2 && !isLaser)
        {
            Instantiate(shot, transform.position + new Vector3(0.75f, 0f, 0f), Quaternion.identity);
            if (isDouble)
                Instantiate(doubleShot, transform.position + new Vector3(0.75f, 0f, 0f), Quaternion.identity);
        }
        else if (Input.GetKeyDown(KeyCode.F) && lasers.Length < 2 && isLaser)
            Instantiate(Laser, transform.position + new Vector3(0.75f, 0f, 0f), Quaternion.identity);
    }
    
    //chagement de la sprite player en fonction de la direction
    void Movement()
    {
        gameObject.GetComponent<SpriteRenderer>().sprite = idle;
        if (Input.GetKey(KeyCode.RightArrow))
        {
            this.transform.position += Vector3.right * speed * Time.deltaTime;
            gameObject.GetComponent<SpriteRenderer>().sprite = idle;
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            this.transform.position += Vector3.left * speed * Time.deltaTime;
            gameObject.GetComponent<SpriteRenderer>().sprite = idle;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            this.transform.position += Vector3.up * speed * Time.deltaTime;
            gameObject.GetComponent<SpriteRenderer>().sprite = go_up;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            this.transform.position += Vector3.down * speed * Time.deltaTime;
            gameObject.GetComponent<SpriteRenderer>().sprite = go_down;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("RedBonus"))
        {
            bonus_up = true;
            bonus_rank += 1;
            if (bonus_rank == 7)
                bonus_rank = 1;
            Destroy(other.gameObject);
        }
    }

    void Bonus_bar()
    {
        if (bonus_rank == 0)
        {
            Speedup_active.SetActive(false);
            Missile_active.SetActive(false);
            Double_active.SetActive(false);
            Laser_active.SetActive(false);
            Option_active.SetActive(false);
            Shield_active.SetActive(false);
        }
        if (bonus_rank == 1 && bonus_up)
        {
            Shield_active.SetActive(false);
            Speedup_active.SetActive(true);
            bonus_up = false;
        }
        else if (bonus_rank == 2 && bonus_up)
        {
            Speedup_active.SetActive(false);
            Missile_active.SetActive(true);
            bonus_up = false;
        }
        else if (bonus_rank == 3 && bonus_up)
        {
            Missile_active.SetActive(false);
            Double_active.SetActive(true);
            bonus_up = false;
        }
        else if (bonus_rank == 4 && bonus_up)
        {
            Double_active.SetActive(false);
            Laser_active.SetActive(true);
            bonus_up = false;
        }
        else if (bonus_rank == 5 && bonus_up)
        {
            Laser_active.SetActive(false);
            Option_active.SetActive(true);
            bonus_up = false;
        }
        else if (bonus_rank == 6 && bonus_up)
        {
            Option_active.SetActive(false);
            Shield_active.SetActive(true);
            bonus_up = false;
        }
    }

    void Bonus_use()
    {
        if (bonus_rank == 1)
        {
            bonus_rank = 0;
            speed += 1;
        }
        else if (bonus_rank == 2)
        {
            bonus_rank = 0;
            //a faire
        }
        else if (bonus_rank == 3)
        {
            isDouble = true;
            bonus_rank = 0;
            isLaser = false;
        }
        else if (bonus_rank == 4)
        {
            bonus_rank = 0;
            isLaser = true;
        }
        else if (bonus_rank == 5)
        {
            bonus_rank = 0;
            //a faire
        }
        else if (bonus_rank == 6)
        {
            bonus_rank = 0;
            //a faire
        }

    }
}




















