﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShot : MonoBehaviour
{
    private int speed;
    void Start()
    {
        if (gameObject.CompareTag("PlayerShot"))
            speed = 10;
        else if (gameObject.CompareTag("PlayerLaser"))
            speed = 20;
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position += Vector3.right * Time.deltaTime * speed;
        
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("ScreenBorder"))
        {
            Destroy(this.gameObject);
        }
    }
}
