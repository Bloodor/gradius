﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleUp : MonoBehaviour
{
    private int speed;

    // Start is called before the first frame update
    void Start()
    {
        speed = 7;
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position += Vector3.right * Time.deltaTime * speed;
        gameObject.transform.position += Vector3.up * Time.deltaTime * speed;


    }
}
